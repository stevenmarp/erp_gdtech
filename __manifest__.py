{
  'name': 'Stevenmarp Technical Odoo Developer',
  'author': 'stevenmarp',
  'version': '1.0.0',
  'depends': [
    'base',
  ],
  'data': [
    'security/ir.model.access.csv',
    'views/guru.xml',
    'views/menu.xml',
    'views/kelas.xml',
    'views/mata_pelajaran.xml',
    'views/siswa.xml',
    'reports/siswa_report.xml',
  ],
  'qweb': [
    # 'static/src/xml/nama_widget.xml',
  ],
  'sequence': 1,
  'auto_install': False,
  'installable': True,
  'application': True,
  'category': '-  Odoo Technical Training V 17',
  'summary': 'Technical Training 17',
  'license': 'LGPL-3',
  'website': ' https://stevenmarpportofolio.wenikah.com/ ',
  'description': '-'
}
