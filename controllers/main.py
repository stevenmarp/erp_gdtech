import odoo.http
from odoo import http
import json


class GdtechCRUD(http.Controller):

        # Customer

    # Read contact
    @http.route('/customer', type='http', auth='none', methods=['GET'])
    def get_customer(self, **kw):
        customer = http.request.env['res.partner'].sudo().search([]).read(['name', 'email', 'phone'],)
        return json.dumps(customer)
    
    # Update contact
    @http.route('/customer/update/<int:id>/<string:name>', type='http', auth='none', methods=['GET'])
    def update_customer(self,id,name, **kw):
        customer = http.request.env['res.partner'].sudo().search([('id','=',id)])
        customer.write({
            'name':name
        })
        return json.dumps("customer %s Updated"%(id))
    
    # Delete contact
    @http.route('/customer/delete/<int:id>', type='http', auth='none', methods=['GET'])
    def delete_customer(self,id, **kw):
        customer = http.request.env['res.partner'].sudo().search([('id','=',id)])
        customer.unlink()
        return json.dumps("customer %s Delete"%(id))
    
    # Create contact
    @http.route('/customer/create/<string:name>', type='http', auth='none', methods=['GET'])
    def create_customer(self,name, **kw):
        customer = http.request.env['res.partner'].sudo()
        customer.create({'name':name})
        return json.dumps("customer %s Create"%(name))


        # Product

    # Read product
    @http.route('/product', type='http', auth='none', methods=['GET'])
    def get_product(self, **kw):
        product = http.request.env['product.product'].sudo().search([]).read(['name'],)
        return json.dumps(product)
    
     # Create contact
    @http.route('/product/create/<int:id>/<string:name>/', type='http', auth='none', methods=['GET'])
    def create_product(self,name, **kw):
        product = http.request.env['product.product'].sudo()
        product.create({'name':name})
        return json.dumps("product %s Create"%(name))


      # Update contact
    # @http.route('/product/update/<int:id>/<string:name>/<int:unit_price>/<string:stock>', type='http', auth='none', methods=['GET'])
    # def update_product(self,id,name,unit_price,stock, **kw):
    #     product = http.request.env['product.product'].sudo().search([('id','=',id)])
    #     product.write({
    #         'name':name,
    #         'unit_price':unit_price,
    #         'stock':stock,
    #     })
    #     return json.dumps("product %s Updated"%(id))


