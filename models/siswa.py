from odoo import models, fields, api



class GdtechSiswa(models.Model):
    _name = 'gdtech.siswa'
    _description = 'Gdtech Siswa'


    name = fields.Char('Name')
    date_of_birth = fields.Date('Tanggal Lahir')
    kelas_id = fields.Many2one('gdtech.kelas', string='Kelas')
    guru_id = fields.Many2one('gdtech.guru', string='Guru')
    mata_pelajaran_id = fields.Many2one('gdtech.matpel', string='Mata Pelajaran' , related='guru_id.matpel_id')

# - name, type char
# - date_of_birth, type date
# - kelas, type Many2one model Kelas
# - guru, type Many2one model Guru
# - mata_pelajaran, type Many2one related ke Model Guru.Mata Pelajaran
