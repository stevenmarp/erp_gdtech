from odoo import models, fields, api



class GdtechGuru(models.Model):
    _name = 'gdtech.guru'
    _description = 'Gdtech Guru'


    name = fields.Char('Name')
    matpel_id = fields.Many2one('gdtech.matpel', string='Mata Pelajaran')

# Model Guru
# Field: - name, type char
# - mapel, type Many2one model Mata Pelajaran
